// list rooms
const ROOMS = {
  members: [
    {
      id: '123148shdjasd-asfhas',
      name: 'STEPaN', 
    },
    {
      id: '37wh0jsd-asjf328hs-sa',
      name: 'Гений',
    },
    {
      id: '42893dh-sjf38112dsa',
      name: 'АйКю999',
    },
  ],
  messages: [
    {id: '123iscxnmo823r', message: "Hi world!", owner: 'STEPaN'},
    {id: '82756bdsijh928hj', message: "Hi bro!", owner: 'Гений'},
    {id: 'skjhdf798235hiuj', message: "Hi.", owner: 'АйКю999'},
    {id: 'asf45asdfcaga2', message: "Hello!!!", owner: 'VladosUPDev'},
  ]
}

module.exports = ROOMS